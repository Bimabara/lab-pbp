1. Apakah perbedaan antara JSON dan XML?

    Json:
        Notasi Objek JavaScript, 
        Tidak menggunakan Tag, 
        Tidak ada comment, 
        Lebih mudah dibaca, 
        Data tersimpan dalam bentuk map (key:Value), 
        Berorientasi pada data.

    Xml:
        Bahasa Markup, 
        Menggunakan tag, 
        Bisa dibubuhi comment, 
        Lebih sulit dibaca ketimbang Json, 
        Data tersimpan dalam bentuk tree, 
        Berorientasi pada dokumen, 









2. Apakah perbedaan antara HTML dan XML?

    HTML:
        HyperText Markup Language atau HTML digunakan untuk menerapkan tata letak dan konvensi pemformatan ke dokumen teks. Bahasa markup membuat teks lebih interaktif dan dinamis yang dapat mengubah teks menjadi gambar, tabel, tautan, dll. Tujuan utamanya adalah menyajikan data dengan format-format tertentu dan tag yang terbatas.

    XML:
        eXtensible Markup Language atau XML merupakan bahasa dinamis yang digunakan untuk mengangkut data dan bukan untuk menampilkan data. Dengan format data tekstual dengan dukungan melalui Unicode untuk berbagai bahasa manusia, desain XML biasanya berfokus pada dokumen. Tetapi bahasa ini banyak digunakan untuk representasi struktur data yang berubah-ubah seperti yang digunakan dalam layanan web. Tujuan utamanya memang transfer data sehingga tidak dapat menyajikan data.