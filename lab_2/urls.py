from django.urls import path, include
from .views import index, xml,json
from django.contrib import admin

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]