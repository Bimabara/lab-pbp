from django import forms
from django.db import models
from django.db.models import fields
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        
        model = Note
        fields = "__all__"