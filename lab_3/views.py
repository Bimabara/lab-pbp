from django.shortcuts import render, redirect
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.urls import reverse


# Create your views here.

@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all()
    response = {'friends' : friends}
    return render(request, 'lab3_index.html', response)



@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}

    form = FriendForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        return redirect('/lab-3')

    context['form'] = form
    return render(request, "lab3_forms.html", context)