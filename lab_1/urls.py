from django.urls import path, include
from .views import index, friend_list
from django.contrib import admin

urlpatterns = [
    # path('', admin.site.urls),
    path('', index, name='index'),
    path('friends', friend_list, name='friend_list')

    # TODO Add friends path using friend_list Views
   
]
