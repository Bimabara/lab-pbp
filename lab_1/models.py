from django.db import models
from django.core.validators import MinLengthValidator



# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10, validators = [MinLengthValidator(10)])
    birth_date = models.DateField()
    # TODO Implement missing attributes in Friend model
